<?php

namespace Drupal\json_api;


/**
 * Class JsonApiParser.
 *
 * @package Drupal\json_api
 */
class JsonApiParser
{
    /**
     * @param $field
     * @param $value
     * @param $nodeLoad
     * @return mixed
     */
    public function fieldSelector($field, $value, $nodeLoad, $state, $language, $apiType)
    {
        $page = null;
        $fieldName = $value['field_name'];

        if ($field['type'] == 'text_with_summary') {
            $page[$fieldName] = $this->textWithSummaryFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'string') {
            $page[$fieldName] = $this->stringFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'boolean') {
            $page[$fieldName] = $this->booleanFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'email') {
            $page[$fieldName] = $this->emailFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'integer') {
            $page[$fieldName] = $this->integerFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'list_float') {
            $page[$fieldName] = $this->listFloatFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'float') {
            $page[$fieldName] = $this->floatFormat($fieldName, $nodeLoad);
        }
        if ($field['type'] == 'datetime') {
            $page[$fieldName] = $this->dateTimeFormat($fieldName, $nodeLoad);
        }
        if ($field['type'] == 'link') {
            $page[$fieldName] = $this->linkFormat($fieldName, $nodeLoad);
        }

        if ($field['type'] == 'entity_reference' && $fieldName != 'field_tags' ) {
            $page[$fieldName] = $this->entityReferenceFormat($fieldName, $nodeLoad, $state, $language, $apiType);
        }

        return $page;
    }


    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function textWithSummaryFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function stringFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function booleanFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function integerFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }
    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function listFloatFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function floatFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function dateTimeFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }
    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function linkFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['url'] = $fieldValue[$key]['uri'];
                $page[$key]['title'] = $fieldValue[$key]['title'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
    public function emailFormat($fieldName, $nodeLoad)
    {
        $fieldValue = $nodeLoad->get($fieldName)->getValue();

        if (is_array($fieldValue)) {
            foreach ($fieldValue as $key => $value) {
                $page[$key]['value'] = $fieldValue[$key]['value'];
            }
        }

        return $page;
    }

    /**
     * @param $fieldName
     * @param $nodeLoad
     * @return mixed
     */
     public function entityReferenceFormat($fieldName, $nodeLoad, $state, $language, $apiType){
         $callbackEntity = \Drupal::service('json_api.manager');
         $fieldValue = $nodeLoad->$fieldName;
        // This works for entity reference / content.


         if ($fieldName == 'field_tags'){
//           var_dump($nodeLoad->$fieldName, $language, $apiType, $state); exit;
         }
         if ($fieldValue) {
     		foreach ($fieldValue as $key => $value) {
//           This should go to a generic method that choose either url or nid. 
     		 $page[$key] = $callbackEntity->apiCreator($fieldValue[$key]->target_id, $language, $apiType, $state);
            }
     	}

//         var_dump($fieldValue[0]->target_id['value']); exit;
//         var_dump($callbackEntity->apiCreator($fieldValue[0]->target_id['value'])); exit;
//         $callbackEntity->apiCreator();

         return $page;
     }
}
