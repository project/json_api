<?php

namespace Drupal\json_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonApiController.
 *
 * @package Drupal\JsonApi\Controller
 */
class JsonApiController extends ControllerBase
{

    /**
     * @return JsonResponse
     */
    public function api()
    {
        return new JsonResponse(\Drupal::service('json_api.manager')->apiBuilder('alias'));
    }

}
