<?php

namespace Drupal\json_api;

use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;
use Drupal\node\Entity\Node;



/**
 * Class JsonApiManager.
 *
 * @package Drupal\json_api.
 */
class JsonApiManager
{

    /**
     * Constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getUrlInfo()
    {
        $parameters = \Drupal::request()->query->all();
        $config = \Drupal::config('system.site');
        // TODO: Investigate why this does not work. 
        // $page['language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();

        if ($parameters['langcode']) {
            $page['language'] = $parameters['langcode'];
        }

        if (empty($parameters['langcode'])) {
            $page['language'] = $config->get('default_langcode');
        }

        if ($parameters['url']) {
            $page['url'] = $parameters['url'];
        }
        if (empty($parameters['url'])) {
            throw new ServiceUnavailableHttpException(3, t('Please enter a URL.'));
        }

        if ($parameters['state']) {
            $page['state'] = $parameters['state'];
        }

        // TODO: Find a more robust way of doing this instead of hardcoring to published.
        if (empty($parameters['state'])) {
            $page['state'] = 'published';
        }
        return $page;
    }

    /**
     * @param $alias
     * @param $language
     * @return int
     */
    public function getNodeIdBaseOnAlias($alias, $language)
    {
        // Getting node id base on alias.
        $query = \Drupal::database()->select('url_alias', 'ua');
        $query->addField('ua', 'source');
        $query->condition('ua.alias', $alias);
        $query->condition('ua.langcode', $language);
        $value = $query->execute()->fetchField();

        if (empty($value)) {
            throw new ServiceUnavailableHttpException(3, t('Please enter a URL.')); // TODO: Change to a better message.
        }

        return intval(preg_replace('/[^0-9]+/', '', $value), 10);
    }

    /**
     * @param $state
     * @param $nid
     * @param $language
     * @return mixed
     */
    public function getWorkflowRevision($state, $nid, $language)
    {
        // Getting getting revision base on the node id, state and $language.
        $query = \Drupal::database()->select('content_moderation_state_field_revision', 'cmstfir');
        $query->addField('cmstfir', 'content_entity_revision_id');
        $query->condition('cmstfir.moderation_state', $state);
        $query->condition('cmstfir.content_entity_id', $nid);
        $query->condition('cmstfir.langcode', $language);
        $query->orderBy('revision_id', 'DESC');

        return $query->execute()->fetchField();

    }

    /**
     * @param $state
     * @param $nid
     * @param $language
     * @return \Drupal\Core\Entity\EntityInterface|null
     */
    public function getRevisionValues($state, $nid, $language)
    {
        $revisionId = $this->getWorkflowRevision($state, $nid, $language);
        return $this->getRevisionNodeLoad($revisionId);
    }

    /**
     * @param $revisionId
     * @return \Drupal\Core\Entity\EntityInterface|null
     */
    public function getRevisionNodeLoad($revisionId)
    {
        // TODO: Consider supporting more entities.
        return \Drupal::entityTypeManager()->getStorage('node')->loadRevision($revisionId);
    }

    public function getNodeLoad($nid)
    {
        // TODO: Consider moving this to entityload so is more scalable.
        return $node = Node::load($nid);
    }

    /**
     * @param $nid
     * @return string
     */
    public function getBundleName($nid)
    {
        $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
        return $node->bundle();
    }

    /**
     * @param $bundle
     * @return mixed
     */
    public function getFieldInfoBaseOnBundle($bundle)
    {
        // Creating an array with fields name and field type.
        // TODO: Find another way to create the entityManager because is depreacted.
        $i = 0;
        foreach (\Drupal::entityManager()->getFieldDefinitions('node', $bundle) as $field_name => $field_definition) {
            if (!empty($field_definition->getTargetBundle())) {
                $bundleFields[$i]['type'] = $field_definition->getType();
                $bundleFields[$i]['field_name'] = $field_name;
                $i++;
            }
        }
        return $bundleFields;
    }


    // TODO: Create a new method that generate $nid, $revision, $bundle $language
    // TODO: Pass these values to the API method

    /**
     * @param $nid
     * @return mixed
     */
    public function apiCreator($nid, $language, $apiType, $state)
    {

        $jsonApiParser = \Drupal::service('json_api.parser');
        $moduleHandler = \Drupal::service('module_handler');

        $fields = $this->getFieldInfoBaseOnBundle($this->getBundleName($nid));
        
        if ($apiType == 'alias'){
            $nodeLoad = $this->getRevisionValues($state, $nid, $language);
        }

        if ($apiType == 'id'){
            $nodeLoad = $this->getNodeLoad($nid);
        }

        if ($moduleHandler->moduleExists('content_translation')){
            $nodeLoad = $nodeLoad->getTranslation($language);
        }

        foreach ($fields as $key => $value) {
            $page[$key] = $jsonApiParser->fieldSelector($fields[$key], $value, $nodeLoad, $state, $language, $apiType);
        }
        // We need to provide the ability of alter this. Either with a hook or event subscriber.
        return $page;
    }

    /**
     * @return mixed
     */
    public function apiBuilder($apiType)
    {
        $urlApi = $this->getUrlInfo();
        $nid = $this->getNodeIdBaseOnAlias($urlApi['url'], $urlApi['language']);
        return $this->apiCreator($nid, $urlApi['language'], $apiType, $urlApi['state']);
    }

}
